README.txt
==========
This module will allow you to pull in data from your Smartlaunch installation via the Smartlaunch API.

Features include:
- System status block
- SSO with user data sync
- View Smartlaunch reports from your Drupal admin dashboard

More functionality will be added later.


AUTHOR/MAINTAINER
======================
David Thomas
http://3digitalrockstudios.com
